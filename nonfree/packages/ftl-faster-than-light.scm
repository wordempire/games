;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;; Copyright © 2019 Efraim Flashner <efraim@flashner.co.il>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages ftl-faster-than-light)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix-gaming build-system binary)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages xorg)
  #:use-module (guix-gaming utils)
  #:use-module (guix-gaming build utils)
  #:use-module (guix-gaming humble-bundle))

(define-public ftl-faster-than-light
  (let* ((version "1.6.9")
         (file-name (string-append "FTL." version ".tar.gz"))
         (binary (match (or (%current-target-system)
                            (%current-system))
                   ("x86_64-linux" "FTL.amd64")
                   ("i686-linux" "FTL.x86")
                   (_ ""))))
    (package
      (name "ftl-faster-than-light")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "0vlgi0kpbybyvrgkdy1mpzhk8h9p2qp5qg1k2irs08izdj66dya4"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:strip-binaries? #f             ; allocated section `.dynstr' not in segment
         #:patchelf-plan
         `((,,(string-append "data/" binary)
            ("libc" "alsa-lib" "libx11" "mesa")))
         #:install-plan
         `(("." ("FTL_README.html") "share/doc/ftl")
           ("data" (,,binary) "bin/ftl")
           ("data" ("ftl.dat") "share/ftl/")
           ("data" ("exe_icon.bmp")  "/share/pixmaps/ftl.bmp")
           ("data" ("licenses") "share/doc/ftl/"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/" "ftl"))
                      (real (string-append (dirname wrapper) "/.ftl")))
                 (rename-file wrapper real)
                 (call-with-output-file wrapper
                   (lambda (p)
                     (format p
                             (string-append
                              "#!" (assoc-ref inputs "bash") "/bin/bash" "\n"
                              "cd " output "/share/ftl" "\n"
                              "exec -a " (basename wrapper) " " real " \"$@\"" "\n"))))
                 (chmod wrapper #o755)
                 (make-desktop-entry-file (string-append output "/share/applications/ftl.desktop")
                                          #:name "FTL: Faster Than Light"
                                          #:exec wrapper
                                          #:icon (string-append output "/share/pixmaps/ftl.bmp")
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("patchelf" ,patchelf)
         ("tar" ,tar)
         ("gzip" ,gzip)))
      (inputs
       `(("alsa-lib" ,alsa-lib)
         ("bash" ,bash-minimal)
         ("libx11" ,libx11)
         ("mesa" ,mesa)))
      (home-page "https://subsetgames.com/ftl.html")
      (synopsis "Space-based top-down real-time strategy roguelike game")
      (description "In FTL you experience the atmosphere of running a spaceship
trying to save the galaxy.  It's a dangerous mission, with every encounter
presenting a unique challenge with multiple solutions.

What will you do if a heavy missile barrage shuts down your shields?

@itemize
@item Reroute all power to the engines in an attempt to escape?
@item Power up additional weapons to blow your enemy out of the sky?
@item Or take the fight to them with a boarding party?
@end itemize

This \"spaceship simulation roguelike-like\" allows you to take your ship and
crew on an adventure through a randomly generated galaxy filled with glory and
bitter defeat.")
      (license ((@@ (guix licenses) license)
                "FTL End User License Agreement"
                "No URL?" "")))))
