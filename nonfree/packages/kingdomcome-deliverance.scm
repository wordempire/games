;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages kingdomcome-deliverance)
  #:use-module (guix-gaming utils)
  #:use-module ((guix licenses) :prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system trivial)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages wine))

;; TODO: Use wine-dxvk.
;; TODO: Containerize.
;; TODO: Add .desktop and icon.
(define-public kingdomcome-deliverance
  (package
    (name "kingdomcome-deliverance")
    (version "0.0.0")
    (source #f)
    (build-system trivial-build-system)
    (inputs
     `(("bash" ,bash-minimal)
       ("wine" ,wine64)
       ("dxvk" ,dxvk)))
    (native-inputs
     `(("kcd-launcher" ,(search-auxiliary-gaming-file "kingdomcome-deliverance"))))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (bash (assoc-ref %build-inputs "bash"))
                (wine (assoc-ref %build-inputs "wine"))
                (dxvk (assoc-ref %build-inputs "dxvk"))
                (launcher (string-append out "/bin/kingdomcome-deliverance")))
           (mkdir-p (string-append out "/bin"))
           (copy-file (assoc-ref %build-inputs "kcd-launcher") launcher)
           (patch-shebang launcher (list (string-append bash "/bin")))
           (substitute* launcher
             (("wine64") (string-append wine "/bin/wine64"))
             (("setup_dxvk") (string-append dxvk "/bin/setup_dxvk"))))
         #t)))
    (home-page "https://www.kingdomcomerpg.com/")
    (synopsis "Story-driven open-world RPG, with historically accurate content")
    (description "Kingdom Come: Deliverance is a story-driven open-world RPG
that immerses you in an epic adventure in the Holy Roman Empire.

The game data must be installed.  It is looked for at the path pointed to by the
KINGDOMCOME_DELIVERANCE_PATH environment variable, which defaults to
$GUIX_GAMING_PATH/Kingdom Come Deliverance.

GUIX_GAMING_PATH defaults to \"~/Games\".

The saves are stored in KINGDOMCOME_DELIVERANCE_SAVE_PATH, which defaults to
\"GUIX_SAVE_PATH/Kingdom Come Deliverance\".

GUIX_SAVE_PATH defaults to XDG_DATA_HOME, which defaults to \"~/.local/share\".

The game uses Wine to run.  The Wine data is stored in
KINGDOMCOME_DELIVERANCE_WINEPREFIX, which defaults to
\"~/.cache/kingdomcome-deliverance\".")
    (supported-systems '("x86_64-linux"))
    ;; TODO: Set proper license.
    (license ((@@ (guix licenses) license) "Unknown license?"
              "No URL"
              ""))))
