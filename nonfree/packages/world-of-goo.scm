;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages world-of-goo)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix-gaming build-system binary)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages xiph)
  #:use-module (guix-gaming utils)
  #:use-module (guix-gaming build utils)
  #:use-module (guix-gaming humble-bundle))

(define-public world-of-goo
  ;; TODO: There is 1.53 as a ".sh" Mojo installer, but it needs more work to
  ;; get it to run on Guix.
  (let* ((version "1.41")
         (file-name (string-append "WorldOfGooSetup." version ".tar.gz"))
         (binary (match (or (%current-target-system)
                            (%current-system))
                   ("x86_64-linux" "WorldOfGoo.bin64")
                   ("i686-linux" "WorldOfGoo.bin32"))))
    (package
      (name "world-of-goo")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "0rj5asx4a2x41ncwdby26762my1lk1gaqar2rl8dijfnpq8qlnk7"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:strip-binaries? #f             ; allocated section `.dynstr' not in segment
         #:patchelf-plan
         `((,,binary
            ("libc" "sdl" "sdl-mixer" "libvorbis" "libogg"
             "gcc" "mesa" "glu")))
         #:install-plan
         `((,,binary #f "share/world-of-goo/")
           ("properties" (".*") "share/world-of-goo/properties/")
           ("res" (".*") "share/world-of-goo/res/")
           ;; TODO: Include icon installation in the build system?
           ("icons" ("16x16.png")  "share/icons/hicolor/16x16/apps/world-of-goo.png")
           ("icons" ("22x22.png")  "share/icons/hicolor/22x22/apps/world-of-goo.png")
           ("icons" ("22x22.png")  "share/icons/hicolor/22x22/apps/world-of-goo.png")
           ("icons" ("32x32.png")  "share/icons/hicolor/32x32/apps/world-of-goo.png")
           ("icons" ("48x48.png")  "share/icons/hicolor/48x48/apps/world-of-goo.png")
           ("icons" ("64x64.png")  "share/icons/hicolor/64x64/apps/world-of-goo.png")
           ("icons" ("128x128.png")  "share/icons/hicolor/128x128/apps/world-of-goo.png")
           ("icons" ("scalable.svg")  "share/icons/hicolor/scalable/world-of-goo.svg")
           ("." ("eula.txt" "readme.html" "linux-issues.txt") "share/doc/world-of-goo/"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/" "world-of-goo"))
                      (real (string-append output  "/share/world-of-goo/" ,binary)))
                 (mkdir-p (dirname wrapper))
                 (call-with-output-file wrapper
                   (lambda (p)
                     (format p
                             (string-append
                              "#!" (assoc-ref inputs "bash") "/bin/bash" "\n"
                              "cd " output "/share/world-of-goo" "\n"
                              "exec -a " (basename wrapper) " " real " \"$@\"" "\n"))))
                 (chmod wrapper #o755)
                 (make-desktop-entry-file (string-append output "/share/applications/world-of-goo.desktop")
                                          #:name "World of Goo"
                                          #:exec wrapper
                                          #:icon (string-append output "/share/icons/hicolor/scalable/apps/world-of-goo.svg")
                                          #:categories '("Application" "Game")))
               #t)))))
      (inputs
       `(("mesa" ,mesa)
         ("glu" ,glu)
         ("gcc" ,(@@ (gnu packages gcc) gcc-9) "lib")
         ("sdl" ,sdl)
         ("sdl-mixer" ,sdl-mixer)
         ("libvorbis" ,libvorbis)
         ("libogg" ,libogg)))
      (home-page "https://2dboy.com/")
      (synopsis "Physics based puzzle / construction game")
      (description "World of Goo is an award winning a physics based puzzle /
construction game.  The millions of Goo Balls who live in the beautiful World of
Goo don't know that they are in a game, or that they are extremely delicious.")
      (license ((@@ (guix licenses) license)
                "WorldOfGoo End User License Agreement"
                "No URL?" "")))))
