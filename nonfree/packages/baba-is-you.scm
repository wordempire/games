;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages baba-is-you)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix-gaming build-system binary)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (guix-gaming utils)
  #:use-module (guix-gaming humble-bundle))

;; TODO: Should baba-is-you be a symbol?  ISO-based setups like Quake 3 may need
;; to support spaces.
(define baba-is-help
  (string-append
    "FETCH FAILED
=============

Please ensure you have set the humble bundle key in `"
    (guix-gaming-channel-games-config)
    "`.

Alternatively, if you didn't obtain the game through humble bundle, you can
download the tarball and build the game with this additional option:
`--with-source=baba-is-you=<path to the tarball file>`.

The key we are talking about is **not** the Steam key.  You can get your key
by going to https://www.humblebundle.com/home/purchases and by selecting
Baba is You.  A new page opens with the key in its URL like this:

https://www.humblebundle.com/downloads?key=abcdefg123456ABC

The key cannot contain any \"&\" character, make sure you don't include the
\"&ttl=\" part.

Please copy that last part of the URL and add it to the configuration file
at `"
    (guix-gaming-channel-games-config)
    "`.

```scheme
((game1 ...)
 ...
 (baba-is-you . ((key . \"abcdefg123456ABC\")))
 ...
 (gameN ...))
```"))

(define-public baba-is-you
  (let ((system (match (or (%current-target-system)
                           (%current-system))
                  ("x86_64-linux" "bin64")
                  ("i686-linux"   "bin32"))))
    (package
      (name "baba-is-you")
      (version "1.0.4")
      (source (origin
               (method humble-bundle-fetch)
               (uri (humble-bundle-reference
                      (help baba-is-help)
                      (config-path '(baba-is-you key))))
               (file-name "BIY_linux_may27.tar.gz")
               (sha256
                (base32
                 "0qy6xpficx7rlvayskfjnqsmly0a3mfkb92mw05mxpsd2l08avfx"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:patchelf-plan
         `((,,(string-append system "/Chowdren")
            ("dbus" "libc" "libxcursor" "libxi" "libxinerama" "libxrandr"
             "libxscrnsaver" "mesa" "pulseaudio")))
         #:install-plan
         `((,,system ("Chowdren") "share/baba-is-you/")
           ("." ("gamecontrollerdb.txt" "Assets.dat") "share/baba-is-you/")
           ("Data" (".*") "share/baba-is-you/Data/")
           ("bin" ("baba-is-you") "bin/"))
         #:phases
         (modify-phases %standard-phases
           (add-before 'install 'create-wrapper
             (lambda* (#:key outputs #:allow-other-keys)
               (mkdir "bin")
               (let ((baba-wrapper "bin/baba-is-you"))
                 (with-output-to-file baba-wrapper
                   (lambda _
                     (format #t "#!~a/bin/bash~%" (which "bash"))
                     (format #t "cd ~a~%" (string-append (assoc-ref outputs "out")
                                                         "/share/baba-is-you"))
                     (format #t "./Chowdren")))
                 (chmod baba-wrapper #o755))
               #t)))))
      (inputs
       `(("dbus" ,dbus)
         ("libxcursor" ,libxcursor)
         ("libxinerama" ,libxinerama)
         ("libxi" ,libxi)
         ("libxrandr" ,libxrandr)
         ("libxscrnsaver" ,libxscrnsaver)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)))
      (home-page "http://www.hempuli.com/baba/")
      (synopsis "Puzzle game where you can change the rules")
      (description "Baba Is You is a puzzle game where the rules you have to
follow are present as physical objects in the game world.  By manipulating the
rules, you can change how the game works, repurpose things you find in the
levels and cause surprising interactions!")
      (license ((@@ (guix licenses) license)
                "Unknown proprietary license"
                "No URL"
                "")))))
