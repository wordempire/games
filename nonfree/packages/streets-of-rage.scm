;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages streets-of-rage)
  #:use-module (guix-gaming utils)
  #:use-module ((guix licenses) :prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system trivial)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages game-development))

(define-public streets-of-rage-remake
  (package
    (name "streets-of-rage-remake")
    (version "5.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "http://launchpad.net/~ubuntugames/"
                                  "+archive/ubuntu/games/+files/sorr_"
                                  version ".orig.tar.gz"))
              (sha256
               (base32
                "1gld4i6ma265j7jw06c1drfp7pbg3n8dpm4lg9xgg28dkl5flwai"))))
    (build-system trivial-build-system)
    (inputs
     `(("bash" ,bash-minimal)
       ("bennu-game-development" ,(to32 bennu-game-development))
       ("bennu-game-development-modules" ,(to32 bennu-game-development-modules))))
    (native-inputs
     `(("tar" ,tar)
       ("gzip" ,gzip)))
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let ((out (assoc-ref %outputs "out")))
           (setenv "PATH" (string-append
                           (assoc-ref %build-inputs "gzip") "/bin:"
                           (assoc-ref %build-inputs "tar") "/bin"))
           (invoke "tar" "xvf" (assoc-ref %build-inputs "source"))
           (mkdir-p out)
           (copy-recursively "." out)
           (chdir out)
           ;; Remove the unneeded executable bit.
           (for-each (lambda (file)
                       (chmod file #o644))
                     (find-files "." ".*"))
           (mkdir-p "share/applications/")
           (let ((icon (string-append out "/share/pixmaps/sorr.png")))
             (substitute* "sorr.desktop"
               (("Icon=sorr.png")
                (string-append "Icon=" icon)))
             (rename-file "sorr.desktop" "share/applications/sorr.desktop")
             (mkdir-p (dirname icon))
             (rename-file "sorr.png" icon))
           (mkdir-p "share/sorr")
           (for-each (lambda (f) (rename-file f (string-append "share/sorr/" f)))
                     '("SorMaker.dat" "SorR.dat" "manual" "manual.html"
                       "mod" "palettes" "Readme.txt"))
           (delete-file "sorr")
           (delete-file "bgdi")
           (delete-file-recursively "bennugd")
           (delete-file-recursively "data")
           (mkdir-p "bin")
           ;; Wrap program to start the game easily:
           (let* ((bash (assoc-ref %build-inputs "bash"))
                  (bennugd (assoc-ref %build-inputs "bennu-game-development"))
                  (bgdi (string-append bennugd "/bin/bgdi"))
                  (bennugd-modules (assoc-ref %build-inputs "bennu-game-development-modules"))
                  (sorr (string-append out "/bin/sorr"))
                  (sorr-data (string-append out "/share/sorr/SorR.dat"))
                  (sorr-path (string-append out "/share/sorr/")))
             (call-with-output-file sorr
               (lambda (p)
                 ;; Passing '-a "$0"' to exec breaks the game.
                 (format p "\
#!~a
export LD_LIBRARY_PATH=~a/lib${LD_LIBRARY_PATH:+:}$LD_LIBRARY_PATH
SORR_PATH=\"${SORR_PATH:-$HOME/.config/sorr}\"
mkdir -p \"$SORR_PATH\"
cd \"$SORR_PATH\"
for i in \"~a\"/*; do
  if [ \"$(basename \"$i\")\" != \"mod\" ]; then
    ln -sf \"$i\"
  fi
done
mkdir -p mod/games
for i in \"~a\"/mod/*; do
  ln -sf \"$i\" mod/
done
exec ~a ~a \"$@\"~%"
                         (string-append bash "/bin/bash")
                         bennugd-modules
                         sorr-path sorr-path
                         bgdi
                         sorr-data)))
             (chmod sorr #o755))
           #t))))
    (home-page "http://www.sorrcommunity.com/t148-sor-v5-for-linux-debian-download-links")
    (synopsis "Remake of the classic Streets of Rage 1, 2 and 3")
    (description "This is a Bombergames remake of the classic Streets of Rage 1,
2 and 3 by Sega.  The save games are stored in ~/.config/sorr (configurable via
the SORR_PATH environment variable).  Mods can be installed in
$SORR_PATH/mod/games.

Note: The game seems to only work with a 32-bit version of Bennu Game
Development.")
    (supported-systems '("i686-linux" "x86_64-linux"))
    (license ((@@ (guix licenses) license) "No license"
              "No URL"
              ""))))
