;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Efraim Flashner <efraim@flashner.co.il>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree packages thomas-was-alone)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix-gaming build-system binary)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (guix-gaming utils)
  #:use-module (guix-gaming build utils)
  #:use-module (guix-gaming humble-bundle))

(define-public thomas-was-alone
  (package
    (name "thomas-was-alone")
    (version "1369349552") ; 23 May 2013 10:52:32 PM UTC
    (source
     (origin
       (method humble-bundle-fetch)
       (uri (humble-bundle-reference
             (help (humble-bundle-help-message name))
             (config-path (list (string->symbol name) 'key))
             (files (list (string-append "thomaswasalone-linux-" version ".tar")))))
       (file-name (string-append "thomaswasalone-linux-" version ".tar"))
       (sha256
        (base32
         "1a48jqw4667nkfb4cfr5kw2dj4wgkiabvzr553hl2kj8cfd4g4ii"))))
    (build-system binary-build-system)
    (supported-systems '("i686-linux" "x86_64-linux"))
    (arguments
     `(#:strip-binaries? #f             ; allocated section `.dynstr' not in segment
       #:system "i686-linux"
       #:patchelf-plan
       `(("thomasWasAlone"
          ("libc" "gcc:lib" "glu" "libx11" "libxcursor" "libxext" "mesa"))
         ("thomasWasAlone_Data/Mono/x86/libmono.so"
          ("gcc:lib")))
       #:install-plan
       `(("." ("thomasWasAlone") "share/thomas-was-alone/")
         ("thomasWasAlone_Data" (".") "share/thomas-was-alone/thomasWasAlone_Data/"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'install 'create-wrapper
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((output (assoc-ref outputs "out"))
                    (wrapper (string-append output "/bin/thomasWasAlone"))
                    (real (string-append output "/share/thomas-was-alone/thomasWasAlone")))
               (mkdir-p (string-append output "/bin"))
               (call-with-output-file wrapper
                 (lambda (p)
                   (format p
                           (string-append
                            "#!" (assoc-ref inputs "bash") "/bin/bash" "\n"
                            "export LD_LIBRARY_PATH="
                            (string-join
                             (map (lambda (lib) (string-append (assoc-ref inputs lib) "/lib"))
                                  '("alsa-lib" "pulseaudio"))
                             ":")
                            "${LD_LIBRARY_PATH:+:}$LD_LIBRARY_PATH" "\n"
                            "cd " output "/share/thomas-was-alone" "\n"
                            "./thomasWasAlone " "\"$@\"" "\n"))))
               (chmod wrapper #o755)
               (chmod real #o555)
               (make-desktop-entry-file
                 (string-append output "/share/applications/thomas-was-alone.desktop")
                 #:name "Thomas was Alone"
                 #:exec wrapper
                 #:categories '("Application" "Game")))
             #t)))))
    (native-inputs
     `(("gzip" ,gzip)
       ("patchelf" ,patchelf)
       ("tar" ,tar)))
    (inputs
     `(("bash" ,bash-minimal)
       ("gcc:lib" ,gcc "lib")
       ("glu" ,glu)
       ("libx11" ,libx11)
       ("libxcursor" ,libxcursor)
       ("libxext" ,libxext)
       ("mesa" ,mesa)
       ("alsa-lib" ,alsa-lib)
       ("pulseaudio" ,pulseaudio)))
    (home-page "http://www.thomaswasalone.com/")
    (synopsis "Minimalist game about friendship and bouncing")
    (description
     "Thomas Was Alone is an indie minimalist 2D platformer about friendship and
jumping and floating and anti-gravity.  Guide a group of rectangles through a
series of obstacles, using their different skills together to get to the end of
each environment.  Thomas Was Alone tells the story of the world's first
sentient AIs, and how they worked together to, well, not escape: Escape is a
strong word.  'Emerge' might be better.  'Emerge' has an air of importance about
it, while keeping the myriad plot twists and superhero origin stories you'll
discover under wraps.  We didn't even mention the bouncing.  That'd be overkill.")
    (license ((@@ (guix licenses) license)
              "Proprietary"
              "No URL?" ""))))
