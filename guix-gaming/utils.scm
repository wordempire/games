;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-gaming utils)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 popen)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:export (getenv*))

(define-public (to32 package64)
  "Build package for i686-linux.
Only x86_64-linux and i686-linux are supported.
- If i686-linux, return the package unchanged.
- If x86_64-linux, return the 32-bit version of the package."
  (match (%current-system)
    ("x86_64-linux"
     (package
       (inherit package64)
       (name (string-append (package-name package64) "32"))
       (arguments `(#:system "i686-linux"
                    ,@(package-arguments package64)))))
    (_ package64)))

(define-public (search-auxiliary-gaming-file file-name)
  "Search the auxiliary FILE-NAME.  Return #f if not found.
This is like SEARCH-AUX-FILE except that it resolves the search path at run
time."
  (search-path (map (cut string-append <> "/nonfree/packages/aux-files")
                    %load-path) file-name))

(define* (getenv* nam #:optional default)
  "Like `getenv' but returns DEFAULT if NAM is not found."
  (or (getenv nam)
      default))

(define-public guix-gaming-path-default (string-append (getenv "HOME") "/Games"))

(define-public guix-gaming-channel-config-directory "/guix-gaming-channels/")
(define-public guix-gaming-channel-games-config-file "games.scm")
(define-public (guix-gaming-channel-games-config)
  "Return the location of the config file.
If a file named `guix-gaming-channel-games-config-file' with a \".gpg\" suffix
is found, it will be automatically decrypted."
  (let* ((xdg-config (getenv "XDG_CONFIG_HOME"))
         (default-config (string-append (getenv "HOME") "/.config"))
         (guix-gaming-config (string-append (or xdg-config default-config)
                                            guix-gaming-channel-config-directory
                                            guix-gaming-channel-games-config-file)))
    (match (string-append guix-gaming-config ".gpg")
      ((? file-exists? file) file)
      (else guix-gaming-config))))

(define (decrypt-and-read file)
  (if (string-suffix? ".gpg" file)
      (let* ((port (open-pipe* OPEN_READ "gpg" "--decrypt" file))
             (str (get-string-all port)))
        (close-pipe port)
        (call-with-input-string str read))
      (call-with-input-file file read)))

(define-public (read-config config-help game . keys)
  (let ((config-file (guix-gaming-channel-games-config)))
    (unless (access? config-file R_OK)
      (error (format #f "File ~a not accessible.~%~a" config-file config-help)))

    (catch #t
           (lambda ()
             (let* ((config (decrypt-and-read config-file))
                    (game-config (or (assoc-ref config game)
                                     (error (format #f "Entry ~a not found.~%~a"
                                                    game config-help)))))
               (let loop ((keys keys) (entry game-config))
                 (match keys
                   ('() entry)
                   ((key keys ...) (loop keys (match (assoc key entry)
                                                ((key . value) value)
                                                (#f (error (format #f "Key '~a' not found" key))))))
                   (else (error (format #f "Key '~a' not found" keys)))))))
           (lambda (key . args)
             (error (format #f "~a~%~a" args config-help))))))
