;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-gaming humble-bundle)
  #:use-module (srfi srfi-1)
  #:use-module (guix-gaming utils)
  #:use-module (gnutls)
  #:use-module ((guix build download) #:hide (url-fetch))
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 match)
  #:use-module (ice-9 ftw)
  #:use-module (json)
  #:use-module (rnrs bytevectors)
  #:use-module (web client)
  #:use-module (web response)
  #:export (humble-bundle-fetch

            humble-bundle-reference
            humble-bundle-reference?
            humble-bundle-reference-config-path
            humble-bundle-reference-help
            humble-bundle-reference-platform))

;; Private methods copied from (guix build download) and specialized for
;; humble bundle.

(define (set-certificate-credentials-x509-trust-file!* cred file format)
  "Like 'set-certificate-credentials-x509-trust-file!', but without the file
name decoding bug described at
<https://debbugs.gnu.org/cgi/bugreport.cgi?bug=26948#17>."
  (let ((data (call-with-input-file file get-bytevector-all)))
    (set-certificate-credentials-x509-trust-data! cred data format)))

(define (peer-certificate session)
  "Return the certificate of the remote peer in SESSION."
  (match (session-peer-certificate-chain session)
    ((first _ ...)
     (import-x509-certificate first x509-certificate-format/der))))

(define (assert-valid-server-certificate session server)
  "Return #t if the certificate of the remote peer for SESSION is a valid
certificate for SERVER, where SERVER is the expected host name of peer."
  (define cert
    (peer-certificate session))

  ;; First check whether the server's certificate matches SERVER.
  (unless (x509-certificate-matches-hostname? cert server)
    (throw 'tls-certificate-error 'host-mismatch cert server))

  ;; Second check its validity and reachability from the set of authority
  ;; certificates loaded via 'set-certificate-credentials-x509-trust-file!'.
  (match (peer-certificate-status session)
    (()                                           ;certificate is valid
     #t)
    ((statuses ...)
     (throw 'tls-certificate-error 'invalid-certificate cert server
            statuses))))

(define (make-credendials-with-ca-trust-files directory)
  "Return certificate credentials with X.509 authority certificates read from
DIRECTORY.  Those authority certificates are checked when
'peer-certificate-status' is later called."
  (let ((cred  (make-certificate-credentials))
        (files (or (scandir directory
                            (lambda (file)
                              (string-suffix? ".pem" file)))
                   '())))
    (for-each (lambda (file)
                (let ((file (string-append directory "/" file)))
                  ;; Protect against dangling symlinks.
                  (when (file-exists? file)
                    (set-certificate-credentials-x509-trust-file!*
                     cred file
                     x509-certificate-format/pem))))
              (or files '()))
    cred))

(define (humble-bundle-port)
  (let ((ai (car (getaddrinfo "hr-humblebundle.appspot.com" "https")))
        (port (socket PF_INET SOCK_STREAM 0)))
    (connect port (addrinfo:addr ai))
    (let ((session  (make-session connection-end/client))
          (ca-certs (%x509-certificate-directory)))
      (set-session-server-name! session server-name-type/dns "hr-humblebundle.appspot.com")
      (set-session-transport-fd! session (fileno port))
      (set-session-default-priority! session)
      ;; FIXME: Since we currently fail to handle TLS 1.3 (with GnuTLS 3.6.5),
      ;; remove it; see <https://bugs.gnu.org/34102>.
      (set-session-priorities! session
                               (string-append
                                "NORMAL:%COMPAT:-VERS-SSL3.0"

                                ;; The "VERS-TLS1.3" priority string is not
                                ;; supported by GnuTLS 3.5.
                                (if (string-prefix? "3.5." (gnutls-version))
                                    ""
                                    ":-VERS-TLS1.3")))
      (set-session-credentials! session
                                (if ca-certs
                                    (make-credendials-with-ca-trust-files
                                     ca-certs)
                                    (make-certificate-credentials)))
      (catch 'gnutls-error
        (lambda ()
          (handshake session))
        (lambda (key err proc . rest)
          (cond ((eq? err error/warning-alert-received)
                 ;; Like Wget, do no stop upon non-fatal alerts such as
                 ;; 'alert-description/unrecognized-name'.
                 (format (current-error-port)
                         "warning: TLS warning alert received: ~a~%"
                         (alert-description->string (alert-get session)))
                 (handshake session))
                (else
                 ;; XXX: We'd use 'gnutls_error_is_fatal' but (gnutls) doesn't
                 ;; provide a binding for this.
                 (apply throw key err proc rest)))))

      ;; Verify the server's certificate if needed.
      (catch 'tls-certificate-error
        (lambda ()
          (assert-valid-server-certificate session "hr-humblebundle.appspot.com"))
        (lambda args
          (close-port port)
          (apply throw args)))

      (let ((record (session-record-port session)))
        ;; Write HTTP requests line by line rather than byte by byte:
        ;; <https://bugs.gnu.org/22966>.  This is possible with Guile >= 2.2.
        (setvbuf record 'line)

        record))))

;; API documentation taken from https://www.schiff.io/projects/humble-bundle-api
;; which is not official.

(define (humble-bundle-get-order key)
  "Get order details"
  (call-with-values
    (lambda ()
      (http-get (string-append "https://hr-humblebundle.appspot.com/api/v1/order/"
                               key)
                #:headers '((X-Requested-By . "hb_android_app"))
                #:port (humble-bundle-port)))
    (lambda (response content)
      (unless (eq? (response-code response) 200)
        (throw 'humble-bundle-exception (format #f "~a error: ~a"
                                                (number->string (response-code response))
                                                (if (string? content)
                                                    content
                                                    (utf8->string content)))))
      (utf8->string content))))

(define* (humble-bundle-download key #:key (platform "linux") (files #f))
  "Return list of direct download URIs machines FILES.
If FILES is #f, all matchines URIs are returned.
Otherwise, the "
  (let* ((info (humble-bundle-get-order key))
         (js (json-string->scm info))
         (products (array->list (assoc-ref js "subproducts")))
         (downloads (apply append (map array->list (map (lambda (p) (assoc-ref p "downloads")) products))))
         (downloads (filter (lambda (dl) (equal? (assoc-ref dl "platform") platform))
                            downloads))
         (downloads (apply append (map array->list (map (lambda (dl) (assoc-ref dl "download_struct"))
                                                        downloads))))
         (downloads (map (lambda (dl) (assoc-ref dl "url")) downloads))
         (downloads (map (lambda (dl) (assoc-ref dl "web")) downloads))
         (file-downloads (if files
                             (filter (lambda (dl)
                                       (any (lambda (f)
                                              (string-contains dl f))
                                            files))
                                     downloads)

                             downloads)))
    (if (and file-downloads (not (null? file-downloads)))
        file-downloads
        (if (and downloads (not (null? downloads)))
            (error "No URI matches" files "in" downloads)
            (error "No download URIs")))))

(define-record-type* <humble-bundle-reference>
  humble-bundle-reference make-humble-bundle-reference
  humble-bundle-reference?
  (config-path humble-bundle-reference-config-path)
  (help        humble-bundle-reference-help)
  (platform    humble-bundle-reference-platform
               (default "linux"))
  (files       humble-bundle-reference-files
               (default #f)))

(define-public (humble-bundle-help-message name)
  (string-append
    "FETCH FAILED
=============

Please ensure you have set the Humble Bundle key in `"
    (guix-gaming-channel-games-config)
    "`.

Alternatively, if you didn't obtain the game through Humble Bundle, you can
download the tarball and build the game with this additional option:
`--with-source="
    name
    "=<path to the tarball file>`.

The key we are talking about is **not** the Steam key.  You can get your key
by going to https://www.humblebundle.com/home/purchases and by selecting
the game.  A new page opens with the key in its URL like this:

https://www.humblebundle.com/downloads?key=abcdefg123456ABC

The key cannot contain any \"&\" character, make sure you don't include the
\"&ttl=\" part.

Please copy that last part of the URL and add it to the configuration file
at `"
    (guix-gaming-channel-games-config)
    "`.

```scheme
((game1 ...)
 ...
 ("
    name
    " . ((key . \"abcdefg123456ABC\")))
 ...
 (gameN ...))
```"))

(define* (humble-bundle-fetch url hash-algo hash
                              #:optional name
                              #:key (system (%current-system))
                              (guile (default-guile)))
  "Return a fixed-output derivation that fetches URL (a humble-bundle-reference
record), which is expected to have hash HASH of type HASH-ALGO (a symbol).
By default, the file name is ``humble-bundle-download''; optionally, NAME can
specify a different file name."
  (unless (humble-bundle-reference? url)
    (error "URI is not a humble-bundle-reference record"))

  (let* ((key (apply read-config
                     (humble-bundle-reference-help url)
                     (humble-bundle-reference-config-path url)))
         (urls (humble-bundle-download key
                                       #:files
                                       (humble-bundle-reference-files url)
                                       #:platform
                                       (humble-bundle-reference-platform url))))
    (url-fetch urls hash-algo hash (or name "humble-bundle-download")
               #:system system #:guile guile)))
